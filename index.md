---
layout: primary
---
{% for language in site.data.languages %}
<h2>{{ language.name }}</h2>
<div class="row row-padded text-center">
  {% for library in language.libraries %}
  <div class="lib-item col-xs-12 col-sm-6 col-md-3">
    <h3><a href="{{ library.name }}">{{ library.name }}</a></h3>
    <p>{{ library.description }}</p>
    <p><a href="https://github.com/zeyla/{{ library.name }}">Repository</a></p>
    <small>Latest Release: <a href="{{ library.name }}/{{ library.latest }}{{ library.tail }}">v{{ library.latest }}</a></small>
    <br>
    <small><a href="{{ library.name }}/latest{{ library.tail }}">Latest commit</a></small>
  </div>
  {% endfor %}
</div>
{% endfor %}
<script>
  var highest = 0;
  var elements = document.getElementsByClassName("col-xs-12");

  for (var x = 0; x < elements.length; x++) {
    var consoleHeight = elements[x].offsetHeight;

    if (consoleHeight > highest) {
        highest = consoleHeight;
    }
  }

  for (var x = 0; x < elements.length; x++) {
    elements[x].style.height = highest + 'px';
  }
</script>
