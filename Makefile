phpdoc:
	cd ./repos/$(repo) && \
	git pull && \
	curl -O http://get.sensiolabs.org/sami.phar && \
	php sami.phar update .sami-config.php && \
	mkdir ./../../public/$(repo)/$(v) -p && \
	cp ./build/* ./../../public/$(repo)/$(v) -a;

phpdocnp:
	cd ./repos/$(repo) && \
	curl -O http://get.sensiolabs.org/sami.phar && \
	php sami.phar update .sami-config.php && \
	mkdir ./../../public/$(repo)/$(v) -p && \
	cp ./build/* ./../../public/$(repo)/$(v) -a;

rustdoc:
	cd ./repos/$(repo) && \
	git pull && \
	cargo doc --no-deps --all-features && \
	mkdir ./../../public/$(repo)/$(v) -p && \
	cp ./target/doc/* ./../../public/$(repo)/$(v) -a;

rustdocnp:
	cd ./repos/$(repo) && \
	cargo doc --no-deps --all-features && \
	mkdir ./../../public/$(repo)/$(v) -p && \
	cp ./target/doc/* ./../../public/$(repo)/$(v) -a;
